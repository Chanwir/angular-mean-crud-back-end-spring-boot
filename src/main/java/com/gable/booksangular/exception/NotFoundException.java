package com.gable.booksangular.exception;

public class NotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotFoundException(String txt, Integer id) {
		super(txt + " id not found : " + id);
	}

}
