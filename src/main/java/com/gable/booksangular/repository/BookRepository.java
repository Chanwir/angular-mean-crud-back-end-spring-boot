package com.gable.booksangular.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gable.booksangular.entity.Book;

public interface BookRepository extends JpaRepository<Book, Integer> {

}
