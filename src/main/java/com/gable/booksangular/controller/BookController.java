package com.gable.booksangular.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gable.booksangular.entity.Book;
import com.gable.booksangular.service.BookService;

@RestController
@RequestMapping("/book")
public class BookController {

	@Autowired
	BookService service;

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("")
	public List<Book> findAll() {
		return service.findAll();
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/read-book/{id}")
	public Book findOne(@PathVariable Integer id) {
		return service.findOne(id);
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("/add-book")
	public Book createBook(@RequestBody Book book) {
		return service.create(book);
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@PutMapping("/update-book/{id}")
	public Book updateBook(@PathVariable Integer id, @RequestBody Book book) {
		return service.update(book, id);
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@DeleteMapping("/delete-book/{id}")
	public ResponseEntity<Object> delete(@PathVariable Integer id) {
		return service.delete(id);
	}

}
