package com.gable.booksangular.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.gable.booksangular.dto.Response;
import com.gable.booksangular.entity.Book;
import com.gable.booksangular.exception.NotFoundException;
import com.gable.booksangular.repository.BookRepository;

@Service
public class BookService {

	@Autowired
	private BookRepository repo;

	public List<Book> findAll() {
		List<Book> books = repo.findAll();
		return books;
	}

	public Book findOne(Integer id) {
		return repo.findById(id).orElseThrow(() -> new NotFoundException("Book", id));
	}

	public Book create(Book data) {
		Book bookSave = repo.save(data);
		return bookSave;
	}

	public Book update(Book data, Integer id) {
		return repo.findById(id).map(x -> {
			x.copy(data);
			return repo.save(x);
		}).orElseGet(() -> {
			throw new NotFoundException("Book", id);
		});
	}

	public ResponseEntity<Object> delete(Integer id) {
		Optional<Book> bookOptional = repo.findById(id);
		if (bookOptional.isPresent()) {
			repo.deleteById(id);
			return Response.responseMessage("Delete book successful", HttpStatus.OK);
		} else {
			throw new NotFoundException("Book", id);
		}
	}

}
