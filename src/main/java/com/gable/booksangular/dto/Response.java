package com.gable.booksangular.dto;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Response {

	public static ResponseEntity<Object> responseMessage(String message, HttpStatus status) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("message", "Delete user successful");
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}

	public static ResponseEntity<Object> responseObject(Object data, String message, HttpStatus status) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("data", data);
		map.put("message", message);
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}
}
