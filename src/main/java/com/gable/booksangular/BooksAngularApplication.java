package com.gable.booksangular;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class BooksAngularApplication {

	public static void main(String[] args) {
		SpringApplication.run(BooksAngularApplication.class, args);
	}

}
